def someDirtyFunc_name(arg1, arg2, arg3, arg4, arg5, arg6):
    res = 0
    for arg in [arg1, arg2, arg3, arg4, arg5, arg6]:
        res += arg
    return res

def empty_func1():
    pass
def another_func(arg1, arg2):
    res = arg1 + arg2
    return res
