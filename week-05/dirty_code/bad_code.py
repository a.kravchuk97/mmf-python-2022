import math
def some_big_operation(some_long_arg, some_another_long_arg_that_longer_that_first):
    return some_long_arg*some_another_long_arg_that_longer_that_first ** some_another_long_arg_that_longer_that_first ** some_another_long_arg_that_longer_that_first
def f(x1,y1, x2,y2):
    result = math.sqrt((x1 -x2)** 2 + (y1- y2) ** 2)
    return result
def get_unique_words(text):
    res = []
    for word in text.split():
        if not word in res:
            res.append(word)
    return res
from time import time
def print_function_elapsed_time(func, *args, **kwargs):
    time_start=time()
    func(*args, **kwargs)
    elapsed = time() - time_start
    print(elapsed)

def get_response(name, surname, patrname):
    resp =  {
        'name': name,
        'surname': surname,
        'patrname': patrname
    }
    return resp
def update_result_with_name_and_surname(result, name, surname):
    #set result name
    result['name'] = name
    #set result surname
    result['surname'] =surname
    return result

def some_logic(text):
    words = get_unique_words(text)
    if len(words) > 1:
        n = len(words) * 2
        fib_num = None
        fib_curr, fib_next = 0, 1
        if n <= 1:
            fib_num = n
        else:
            for i in range(n):
                fib_curr, fib_next = fib_next, fib_curr + fib_next
            fib_num = fib_curr
            # if fib_num == 10:
            #     print('---debug---')
            #     print(words)
            #     print('---debug---')
    if len(words) == 1:
        n = len(words)
        fib_num = None
        fib_curr, fib_next = 0, 1
        if n <= 1:
            fib_num = n
        else:
            for i in range(n):
                fib_curr, fib_next = fib_next, fib_curr + fib_next
            fib_num = fib_curr
    else:
        fib_num = 0
    if fib_num==None:
        raise Exception('alarm!!')
    result = {
        'fib_num': fib_num
    }
    if len(words)==2:
        update_result_with_name_and_surname(result, words[0], words[1])
    return result
print_function_elapsed_time(some_logic('kravchuk artem'))
